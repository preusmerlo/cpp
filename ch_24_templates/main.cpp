#include <iostream>
#include <string>
#include <cstring>

template <typename T>
T maximum(T a, T b);

template <>
const char* maximum<const char*> (const char*, const char*);

template <typename T>
void sum(T a, T b, T& c);

int main(int argc, char const *argv[])
{

    int int_a {10};
    int int_b {15};
    int int_c {};

    double double_a {10.25};
    double double_b {15.90};
    double double_c {};

    /* Automatic type deduction */
    int_c = maximum(int_a, int_b);

    /* User type */
    int_c = maximum<int>(int_a, int_b);
    double_c = maximum<double>(double_a, double_b);

    std::cout << "Result int c: " << int_c << std::endl;
    std::cout << "Result double c: " << double_c << std::endl;

    /* Pass values by reference */
    std::string str_a {"Hola mundo"};
    std::string str_b {"Hola mundo"};
    std::string str_c {};

    sum<std::string>(str_a, str_b, str_c);
    std::cout << "Result string c: " << str_c << std::endl;

    /* Specialized templates */
    const char* cstring_a {"Hola mundo"};
    const char* cstring_b {"Hola mundoooooooooooooooooo"};
    const char* cstring_c = maximum(cstring_a, cstring_b);

    std::cout << "Result string c: " << cstring_c << std::endl;

    return 0;
}

/* General template */
template <typename T>
T maximum(T a, T b){
    return (a > b)? a : b;
}

/* Specialized template for strings */
template <>
const char* maximum<const char*> (const char* a, const char* b){
    return (std::strcmp(a, b) > 0)? a : b;
}

/* Template with data by reference */
template <typename T>
void sum(T a, T b, T& c){
    c = a + b;
}