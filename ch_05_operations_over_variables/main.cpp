#include <iostream>
#include <cmath>

int main(int argc, char const *argv[])
{

    /* Basic operations */
    
    std::cout << "Basic Operations (+, ., /, %)" << std::endl;

    int a {32};
    int b {10};
    
    int c {a + b};
    int d {a - b};
    int e {a * b};
    int f {a / b};
    int g {a % b};

    std::cout << "a: " << a << std::endl;
    std::cout << "b: " << b << std::endl;
    std::cout << "c: " << c << std::endl;
    std::cout << "d: " << d << std::endl;
    std::cout << "e: " << e << std::endl;
    std::cout << "f: " << f << std::endl;
    std::cout << "g: " << g << std::endl;

    /* Ingrement and decrement */
    
    std::cout << "Increment / Decrement" << std::endl;

    /* 
        Postfix increment/decrement (var++ / var--)
            Orden de ejecucion
                1) Usar / Asignar
                2) Incrementar / Decrementar

        Prefix increment/decrement (++var / --var)
            Orden de ejecucion
                1) Incrementar / Decrementar
                2) Usar / Asignar
    */ 


    int value {5};
    std::cout << "First print: " << value++ << std::endl;
    std::cout << "Second print: " << value << std::endl;

    value = 5;
    std::cout << "First print: " << ++value << std::endl;
    std::cout << "Second print: " << value << std::endl;
    
    /* Floor(to -inf) / Ceil(to +inf) / Round (nearest int) */
    double weight {-0.2};
    std::cout << "weight rounded to floor is : " << std::floor(weight) << std::endl;
    std::cout << "weight rounded to ceil is : " << std::ceil(weight) << std::endl;
    std::cout << "weight rounded is : " << std::round(weight) << std::endl;

    /* Module */
    std::cout << "Abs of +10.12 is : " << std::abs(+10.12) << std::endl;
    std::cout << "Abs of -10.12 is : " << std::abs(-10.12) << std::endl;
    
    /* Exponential */
    std::cout << "The exponential of 10 is : " << std::exp(10) << std::endl;
    
    /* Power */
    std::cout << "3^4 is : " << std::pow(3, 4) << std::endl;
    std::cout << "9^3 is : " << std::pow(9, 3) << std::endl;

    /* Square Root */
    std::cout << "The square root of 81 is : " << std::sqrt(81) << std::endl;

    /* Log_{e} */
    std::cout << "Log_e(54.59): " << std::log(54.59) << std::endl;
    
    /* Log_{10} */
    std::cout << "Log_{10}(10000): " << std::log10(10000) << std::endl; // 4

    return 0;
}
