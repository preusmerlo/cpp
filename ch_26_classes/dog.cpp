#include "dog.h"
#include <iostream>

Dog::Dog(std::string_view name, std::string_view breed, const int age){
    this->name = name;
    this->breed = breed;
    this->p_age = new int {age};
    std::cout << "Constructor called for: " << this->name << std::endl;
}

Dog::~Dog(){
    delete this->p_age;
    this->p_age = nullptr;
    std::cout << "Destructor called for: " << this->name << std::endl;
}

std::string_view Dog::get_name(){
    return this->name;
}

std::string_view Dog::get_breed(){
    return this->breed;
}

int Dog::get_age(){
    return *(this->p_age);
}

Dog* Dog::set_name(std::string_view name){
    this->name = name;
    return this;
}

Dog* Dog::set_breed(std::string_view name){
    this->breed = name;
    return this;
}

Dog* Dog::set_age(int age){
    *(this->p_age) = age;
    return this;
}

Dog& Dog::set_by_ref_name(std::string_view name){
    this->name = name;
    return *this; // Desreference and return
}

Dog& Dog::set_by_ref_breed(std::string_view name){
    this->breed = name;
    return *this; // Desreference and return
}

Dog& Dog::set_by_ref_age(int age){
    *(this->p_age) = age;
    return *this; // Desreference and return
}