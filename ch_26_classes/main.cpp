/**
 * @file main.cpp
 * @author Patricio Reus Merlo (patricio.reus.merlo@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2023-11-03
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <iostream>
#include "cylinder.h"
#include "dog.h"

Dog rename_dog(std::string_view name, Dog dog);

int main(int argc, char const *argv[]){

    /* Definition */
    Cylinder cylinder(10, 20);
    std::cout << "Volume: " << cylinder.volume() << std::endl;
    
    /* Setters and getters */
    cylinder.set_base_radius(5);
    cylinder.set_height(5);

    /* Methods */
    std::cout << "Volume: " << cylinder.volume() << std::endl;

    /* Objects and pointer */
    Cylinder c1(11, 32);                    // Memory allocated on the stack
    Cylinder* c2 = new Cylinder(22, 15);    // Memory allocated on the heap
    Cylinder* p_c1 {&c1};

    /* There are 2 ways to access to the object methods:
        1) (* ).method_name()
        2) ->method_name()
    */

    std::cout << "Volume of C2 through object : " << c1.volume() << std::endl;
    std::cout << "Volume of C2 through pointer: " << p_c1->volume() << std::endl;
    
    std::cout << "Volume of C2 through (*).: " << (*c2).volume() << std::endl;
    std::cout << "Volume of C2 through  -> : " << c2->volume() << std::endl;

    delete c2;
    c2 = nullptr;

    /* Constructor and destructor 
    {
        Dog dog_1("Pepito", "de la calle", 10);
        Dog* dog_2 = new Dog("Juancito", "de la calle", 15);    

        std::cout << "ID (" << &dog_1 << ") desde el main (" << dog_1.get_name() << ")" << std::endl;

        Dog dog_3 {rename_dog("Carlitos", dog_1)};

        std::cout << "ID (" << &dog_1 << ") desde el main (" << dog_1.get_name() << ")" << std::endl;
        std::cout << "ID (" << &dog_2 << ") desde el main (" << dog_2->get_name() << ")" << std::endl;
        std::cout << "ID (" << &dog_3 << ") desde el main (" << dog_3.get_name() << ")" << std::endl;

        delete dog_2;
        dog_2 = nullptr;
    }
    */

    {
        /* El constructor se llama en orden */
        
        Dog dog_1("Dog 1", "de la calle", 10);
        Dog dog_2("Dog 2", "de la calle", 10);
        Dog dog_3("Dog 3", "de la calle", 10);
        Dog dog_4("Dog 4", "de la calle", 10);
        Dog dog_5("Dog 5", "de la calle", 10);

        /* El destructor se llama en orden reverso
           (el ultimo objeto creado es el primero destruido)
        */
    }

    /* This pointer */

    Dog* p_dog = new Dog("Perrito", "de la calle", 12);

    std::cout << "(" << p_dog << ") desde el main: "
              << p_dog->get_name() << " " << p_dog->get_breed() << std::endl;
    
    /* Nested calls ussing "this" pointer (nested calls with arrow (->) operator)
       (each "set" function returns the "this" pointer which allows to the object to nest calls)
    */
    p_dog->set_name("Otro perrito")->set_breed("pero este no es de la calle");

    std::cout << "(" << p_dog << ") desde el main: "
              << p_dog->get_name() << " " << p_dog->get_breed() << std::endl;
    
    /* Nested calls ussing references (nested calls with dot (.) operator)
       (each "set" function returns the "this" value which allows to the object to nest calls)
    */

    p_dog->set_by_ref_name("Otro perritooooooooo").set_by_ref_breed("pero este SI es de la calle");

    std::cout << "(" << p_dog << ") desde el main: "
              << p_dog->get_name() << " " << p_dog->get_breed() << std::endl;

    delete p_dog;
    p_dog = nullptr;


    return 0;
}

Dog rename_dog(std::string_view name, Dog dog){
    std::cout << "ID (" << &dog << ") desde la funcion (" << dog.get_name() << ")" << std::endl;
    Dog aux_dog(name, dog.get_breed(), dog.get_age());
    std::cout << "ID (" << &aux_dog << ") desde la funcion (" << aux_dog.get_name() << ")" << std::endl;
    return aux_dog;
}
