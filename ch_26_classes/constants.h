/**
 * @file constants.h
 * @author Patricio Reus Merlo (patricio.reus.merlo@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2023-11-03
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H

constexpr long double PI {3.14159265359L};

#endif