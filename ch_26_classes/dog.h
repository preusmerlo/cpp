/**
 * @file dog.h
 * @author Patricio Reus Merlo (patricio.reus.merlo@gmail.com)
 * @brief Dog class header.
 * @version 0.1
 * @date 2023-11-06
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef DOG_H
#define DOG_H
#include <string>
#include <string_view>

class Dog{
    private:
        std::string name;
        std::string breed;
        int* p_age {nullptr};

    public:
        Dog() = default;
        Dog(std::string_view name, std::string_view breed, const int age);
        ~Dog();

        std::string_view get_name();
        std::string_view get_breed();
        int get_age();

        Dog* set_name(std::string_view name);
        Dog* set_breed(std::string_view name);
        Dog* set_age(int age);

        Dog& set_by_ref_name(std::string_view name);
        Dog& set_by_ref_breed(std::string_view name);
        Dog& set_by_ref_age(int age);

};

#endif
