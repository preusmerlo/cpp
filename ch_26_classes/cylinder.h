/**
 * @file cylinder.h
 * @author Patricio Reus Merlo (patricio.reus.merlo@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2023-11-03
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef CYLINDER_H
#define CYLINDER_H

#include <concepts>
#include "constants.h"

template<typename T>
concept Numeric = std::is_integral_v<T> || std::is_floating_point_v<T>;

class Cylinder{
    private:
        double base_radius {1.0};
        double height {1.0};

    public:
        Cylinder() = default;
        
        template<typename T>
        Cylinder(T base_radius, T height) requires Numeric<T>;

        double volume();

        double base_area();

        double get_base_radius();

        double get_height();

        template<typename T>
        void set_base_radius(T base_radius) requires Numeric<T>;

        template<typename T>
        void set_height(T height) requires Numeric<T>;
};


#include "cylinder.cpp"
#endif