template<typename T>
Cylinder::Cylinder(T base_radius, T height) requires Numeric<T>{
    this->base_radius = base_radius;
    this->height = height;
}

double Cylinder::volume(){
    return base_area() * height;
}

double Cylinder::base_area(){
    return PI * base_radius * base_radius;
}

double Cylinder::get_base_radius(){
    return base_radius;
}

double Cylinder::get_height(){
    return height;
}

template<typename T>
void Cylinder::set_base_radius(T base_radius) requires Numeric<T>{
    this->base_radius = base_radius;
    this->height = height;
}

template<typename T>
void Cylinder::set_height(T height) requires Numeric<T>{
    this->height = height;
}