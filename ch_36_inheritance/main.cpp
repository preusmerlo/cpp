#include <iostream>
#include <string>
#include <string_view>

/**
 * @brief Person class
 * 
 */
class Person{
    private:
        std::string first_name {};
        std::string last_name {};
        unsigned short int age {};

    public:
        
        /**
         * @brief Construct a new Person object (default)
         * 
         */
        Person () = default;
        
        /**
         * @brief Construct a new Person object
         * 
         * @param first_name 
         * @param last_name 
         * @param age 
         */
        Person(std::string_view first_name, 
               std::string_view last_name, 
               unsigned short int age);

        /**
         * @brief Destroy the Person object
         * 
         */
        ~Person() = default;
        
        /**
         * @brief Set the first name object
         * 
         * @param first_name 
         * @return Person& 
         */
        Person& set_first_name(std::string_view first_name);
        Person& set_last_name(std::string_view last_name);
        Person& set_age(unsigned short int age);

        std::string_view get_first_name() const;
        std::string_view get_last_name() const;
        unsigned short int get_age() const;

};

Person::Person(std::string_view first_name, 
               std::string_view last_name,
               unsigned short int age){

    this->first_name = first_name;
    this->last_name = last_name;
    this->age = age;
}

Person& Person::set_first_name(std::string_view first_name){
    this->first_name = first_name;
    return *this;
}

Person& Person::set_last_name(std::string_view last_name){
    this->last_name = last_name;
    return *this;
}

Person& Person::set_age(unsigned short int age){
    this->age = age;
    return *this;
}

std::string_view Person::get_first_name() const {
    return this->first_name;
}

std::string_view Person::get_last_name() const {
    return this->last_name;
}

unsigned short int Person::get_age() const {
    return this->age;
}

/**
 * @brief Player class
 * 
 */
class Player : public Person {
    private:
        std::string sport;
        
    public:
        /**
         * @brief Construct a new Player object
         * 
         * @param first_name 
         * @param last_name 
         * @param age 
         * @param sport 
         */
        Player(std::string_view first_name, 
               std::string_view last_name,
               unsigned short int age, 
               std::string_view sport);

        /**
         * @brief Destroy the Player object
         * 
         */
        ~Player() = default;

        std::string_view get_sport() const;

        friend std::ostream& operator << (std::ostream& cout, const Player& obj);
};

Player::Player(std::string_view first_name, 
               std::string_view last_name,
               unsigned short int age, 
               std::string_view sport){

    this->sport = sport;
    this->set_first_name(first_name);
    this->set_last_name(last_name);
    this->set_age(age);
}

std::string_view Player::get_sport() const{
    return this->sport;
}

std::ostream& operator<<(std::ostream& cout, const Player& obj){
    cout << "[" << obj.get_first_name() << "]" 
         << "[" << obj.get_last_name() << "]"
         << "[" << obj.get_age() << "]"
         << "[" << obj.get_sport() << "]" << std::endl;
    return cout;
}

int main(int argc, char const *argv[]){
    
    Player p1("Juan", "Domingo", 10, "Politica");
    std::cout << p1 << std::endl;

    return 0;
}
