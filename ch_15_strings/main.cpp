#include <iostream>
#include <cctype>
#include <cstring>
#include <string>

/*
    Funciones in line (pending)
    Indexado de posiciones de memoria adyacente (rows)
    Definir variables en el scope mas chico posible
    Usar llaves para reducir el scope de las variables {}
*/

int main(int argc, char const *argv[])
{

    /* Determine things about characters */

    unsigned char letra {'C'};
    unsigned char numero {'1'};
    unsigned char simbolo {'^'};
    unsigned char blank {' '};
    unsigned char uppercase {'A'};
    unsigned char lowercase {'a'};

    std::cout << "Is alphanumeric: " << (bool) std::isalnum(letra) << std::endl;
    std::cout << "Is alphanumeric: " << (bool) std::isalnum(numero) << std::endl;
    std::cout << "Is alphanumeric: " << (bool) std::isalnum(simbolo) << std::endl;

    std::cout << "Is alphabetic: " << (bool) std::isalpha(letra) << std::endl;
    std::cout << "Is alphabetic: " << (bool) std::isalpha(numero) << std::endl;
    std::cout << "Is alphabetic: " << (bool) std::isalpha(simbolo) << std::endl;

    std::cout << "Is digit: " << (bool) std::tolower(uppercase) << std::endl;
    std::cout << "Is digit: " << (bool) std::toupper(lowercase) << std::endl;

    std::cout << "Is blank: " << (bool) std::isblank(blank) << std::endl;
    
    std::cout << "To lower: " << (unsigned char) std::tolower(uppercase) << std::endl;
    std::cout << "To upper: " << (unsigned char) std::toupper(lowercase) << std::endl;

    /* C-string size */

    const char mensaje_1 [] {"Hola mundoooooooooooooooooooooooooo!"};
    const char* mensaje_2 {"Hola mundoooooooooooooooooooooooooo!"};

    std::cout << "Mensaje 1: " << mensaje_1 << "(" << std::strlen(mensaje_1) << ")" << std::endl; /* string length WITHOUT \0 */
    std::cout << "Mensaje 1: " << mensaje_1 << "(" << sizeof(mensaje_1) << ")" << std::endl;      /* string length WITH \0 */

    std::cout << "Mensaje 2: " << mensaje_2 << "(" << std::strlen(mensaje_2) << ")" << std::endl; /* string length WITHOUT \0 */
    std::cout << "Mensaje 2: " << mensaje_2 << "(" << sizeof(mensaje_2) << ")" << std::endl;      /* POINTER SIZE IN BYTES !!! */

    /* Compare strings */

    const char* data_1 {"Pedrito"};
    const char* data_2 {"Pedrito123"};

    std::cout << "Strings N compare: "<< std::strncmp(data_1, data_2, std::strlen(data_2)) << std::endl;


    /* C++ string (from <string>) */

    std::string pepe;                       // Empty string
    std::string msj_1 {"Hola mundo"};       // Initialized string
    std::string msj_2 {msj_1};              // Init msj_2 with the content of msj_1
    std::string msj_3 {"Hola mundo", 4};    // Init with the first 4 elements of "Hola mundo"
    std::string msj_4 (4, '4');             // Init with a copy of char element (4 copies of '4')
    std::string msj_5 {msj_1, 3, 5};        // Init with a slice of other string (from 3, take 5 elements)

    std::cout << "msj 1: " << msj_1 << std::endl;
    std::cout << "msj 2: " << msj_2 << std::endl;
    std::cout << "msj 3: " << msj_3 << std::endl;
    std::cout << "msj 4: " << msj_4 << std::endl;
    std::cout << "msj 5: " << msj_5 << std::endl;

    return 0;
}