#include <iostream>

int main(int argc, char const *argv[])
{

    /* Declare and use pointers */
    int * p_numer {nullptr};
    double * p_fact_number {nullptr};
    
    std::cout << p_numer << std::endl;
    std::cout << p_fact_number << std::endl;

    /* Pointers have the same size regardless the data type they are pointing */
    std::cout << "int * size:" << sizeof(p_numer) << std::endl;
    std::cout << "double * size:" << sizeof(p_fact_number) << std::endl;

    /* Pointer value */
    
    int a {10};
    int *p_a {nullptr};

    p_a = &a;
    
    std::cout << "Pointer (value):" << p_a << std::endl;
    std::cout << "Pointer (data):" << *p_a << std::endl;

    /* Pointer to char variable */
    char char_var {'A'};
    char *p_char_var {&char_var};
    
    const char *p_char_array {"Hola mundo!!"};
    /* Este tipo de inicializacion (ultimo) solo soporta const char */

    std::cout << "Value of char_var:" << *p_char_var << std::endl;
    std::cout << "Value of char_array with *: " << *p_char_array << std::endl;
    std::cout << "Value of char_array without *: " << p_char_array << std::endl;

    /* Dynamic Memory Allocation */
    
    int *p_dyn_int_1 {nullptr};
    int *p_dyn_int_2 {nullptr};
    int *p_dyn_int_3 {new int {30}};
    
    p_dyn_int_1 = new int {20}; /* memory allocation with initialization */
    p_dyn_int_2 = new int;      /* memory allocation without initialization */

    std::cout << "Value of p_dyn_int_1: " << p_dyn_int_1 << std::endl;
    std::cout << "Value in p_dyn_int_1: " << *p_dyn_int_1 << std::endl;

    std::cout << "Value of p_dyn_int_2: " << p_dyn_int_2 << std::endl;
    std::cout << "Value in p_dyn_int_2: " << *p_dyn_int_2 << std::endl;
    *p_dyn_int_2 = 17;
    std::cout << "Value in p_dyn_int_2: " << *p_dyn_int_2 << std::endl;

    std::cout << "Value of p_dyn_int_3: " << p_dyn_int_3 << std::endl;
    std::cout << "Value in p_dyn_int_3: " << *p_dyn_int_3 << std::endl;
    
    /* Dynamic Memory Release */

    delete p_dyn_int_1;
    p_dyn_int_1 = nullptr;

    delete p_dyn_int_2;
    p_dyn_int_2 = nullptr;

    delete p_dyn_int_3;
    p_dyn_int_3 = nullptr;
    
    /* Buenas practicas:
        - Siempre inicializar los punteros
        - Luego de liberar memoria (delete), redefinir a el puntero original
        - En caso de multiples punteros a una direccion de memoria dinamia, debe
          ser claro cual es el puntero master a partir del cual se aloco la memoria.
          Si se usa delete con ese puntero, todos los punteros slave dejan de ser validos.
          Para verificar esto se puede comparar el puntero master con nullptr
    */

    int * p_master {new int {30}};
    int * p_slave {p_master};

    std::cout << std::endl;
    std::cout << "Value in p_master (" << p_master << "): " << *p_master << std::endl;
    std::cout << "Value in p_slave (" << p_slave << "): " << *p_slave << std::endl;
    
    delete p_master;
    p_master = nullptr;
    
    std::cout << "Value in p_slave (" << p_slave << "): " << *p_slave << std::endl;

    if (p_master == nullptr)
    {
        std::cout << "Value in p_slave (" << p_slave << ") is not longer valid" << std::endl;
    }
    
    /* Check if new could allocate memory */

    int *p_data {new(std::nothrow) int {30}};
    
    if(p_data != nullptr){ 
        std::cout << "Memory was allocated well" << std::endl;
    }

    if(p_data){  /* --> equivalent to the previous if condition */
        std::cout << "Memory was allocated well" << std::endl;
    }

    /* Different ways to declare and initialize an array */

    size_t size {10};
    double *p_salaries {new double[size]};                              /* Only declaration */
    int *p_students {new(std::nothrow) int[size]{}};                    /* Declaration + initialization with 0 */
    double *p_scores {new(std::nothrow) double[size]{1, 2, 3, 4, 5}};   /* Declaration + initialization with some values */

    if(p_salaries && p_students && p_scores)
    {
        for(size_t i {}; i < size; i++)
        {
            std::cout << "Salaries ["<< i <<"]: " << p_salaries[i] << std::endl;
            std::cout << "Students ["<< i <<"]: " << p_students[i] << std::endl;
            std::cout << "Scores ["<< i <<"]: " << p_scores[i] << std::endl;
        }
    }

    /* Delete dynamic arrays */
    delete[] p_salaries;
    p_salaries = nullptr;

    delete[] p_students;
    p_students = nullptr;

    delete[] p_scores;
    p_scores = nullptr;

    /* Static array vs dynamic arrays */
    int static_array [] {1, 2, 3, 4, 5, 6};
    int *p_dynamic_array {new(std::nothrow) int[] {1, 2, 3, 4, 5, 6}};

    /* Static arrays support std::size() and for range loops */
    /* Dynamic arrays DO NOT support std::size() and for range loops */

    for (auto value : static_array)
    {
        std::cout << "Value: " << value << std::endl;
    }

    /* ERROR    
        for (auto value : p_dynamic_array)
        {
            std::cout << "Value: " << value << std::endl;
        }
    */

    for (size_t i {}; i < std::size(static_array); i++)
    {
        std::cout << "Value [" << i << "]:" << static_array[i] << std::endl;
    }

    /* ERROR
        for (size_t i {}; i < std::size(p_dynamic_array); i++)
        {
            std::cout << "Value [" << i << "]:" << p_dynamic_array[i] << std::endl;
        }
    */

    return 0;
}
