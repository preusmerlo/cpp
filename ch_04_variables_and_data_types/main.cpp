#include <iostream>
#include <iomanip>

int main(int argc, char const *argv[])
{

    /* Hola mundo */
    std::cout << "Hola mundo!" << std::endl;

    /* Variables int */

    std::cout << "Notacion numerica" << std::endl;
    
    int number1 = 15;            // Decimal
    int number2 = 017;           // Octal
    int number3 = 0x0f;          // Hexadecimal
    int number4 = 0b00001111;    // Binario

    std::cout << "N1: " << number1 << std::endl;
    std::cout << "N2: " << number2 << std::endl;
    std::cout << "N3: " << number3 << std::endl;
    std::cout << "N4: " << number4 << std::endl;
    
    /* Assignment Initialization */
    
    std::cout << "Inicializacion con = " << std::endl;

    int a = 10.5;       // Inicializacion CON casteo (a = 10)
    double b = 10.5;    // Inicializacion CON casteo (b = 10.5)

    std::cout << "a: " << a << std::endl;
    std::cout << "b: " << b << std::endl;

    /* Variable Braced Initialization */

    std::cout << "Inicializacion con {} " << std::endl;

    int c {10};         // Inicializacion SIN casteo (c = 10)
    double d {10.5};    // Inicializacion SIN casteo (d = 10.5)
                        // int d {10.5} da error de compilacion

    std::cout << "c: " << c << std::endl;
    std::cout << "d: " << d << std::endl;

    /* Functional Variable  Initialization */

    std::cout << "Inicializacion con () " << std::endl;

    int e(10);          // Inicializacion CON casteo (e = 10)
    double f(10.5);     // Inicializacion CON casteo (f = 10.5)
    int g(10.5);        // Inicializacion CON casteo (g = 10)

    std::cout << "e: " << e << std::endl;
    std::cout << "f: " << f << std::endl;
    std::cout << "g: " << g << std::endl;

    /* Zero and Random Initialization */
    
    std::cout << "Inicializacion con 0 vs random" << std::endl;

    int y;              // Inicializacion con basura (y = random)
    int x {};           // Inicializacion con 0 (x = 0)
    
    std::cout << "y: " << y << std::endl;
    std::cout << "x: " << x << std::endl;

    /* Size of a type in memory */

    std::cout << "Data size in bytes" << std::endl;

    bool var_bool {true};
    char var_char {'p'};
    int var_int {10};
    float var_float {20.2};
    double var_double {20.2};

    std::cout << "bool in bytes: " << sizeof(var_bool) << std::endl;
    std::cout << "char in bytes: " << sizeof(var_char) << std::endl;
    std::cout << "int in bytes: " << sizeof(var_int) << std::endl;
    std::cout << "float in bytes: " << sizeof(var_float) << std::endl;
    std::cout << "double in bytes: " << sizeof(var_double) << std::endl;
    
    /* Signed vs Unsigned */

    std::cout << "Signed and unsigned data" << std::endl;

    int var_only_int {-10};                 // Implicitamente signado
    signed int var_signed_int {-10};        // Explicitamente signado
    unsigned int var_unsigned_1_int {1};     // No signado (SIN cast)
    unsigned int var_unsigned_2_int = -1;    // No signado (CON cast)

    std::cout << "int: " << var_only_int << std::endl;
    std::cout << "signed int: " << var_signed_int << std::endl;
    std::cout << "unsigned int: " << var_unsigned_1_int << std::endl;
    std::cout << "unsigned int (CON cast): " << var_unsigned_2_int << std::endl;

    /* Size Modifiers of Integer Type */
    
    // Int = 4 bytes
    int var_int_ {10};
    signed var_signed_ {10};
    signed int var_signed_int_ {10};
    unsigned int var_unsigned_short_int_ {10};

    // Short = 2 bytes
    short var_short {10};
    short int var_short_int {10};
    signed short var_signed_short {10};
    signed short var_unsigned_short {10};
    signed short int var_signed_short_int {10};
    unsigned short int var_unsigned_short_int {10};

    // Long = 4 bytes or 8 bytes (reemplazar short por long)
    // Long long = 8 bytes (reemplazar short por long long)

    /* Fractional Numbers */

    float var_float_4_bytes {1.12345678901234567890F};
    double var_double_8_bytes {1.12345678901234567890};
    long double var_long_double_16_bytes  {1.12345678901234567890L};

    std::cout << std::setprecision(22);
    std::cout << "float: " << var_float_4_bytes << std::endl;
    std::cout << "double: " << var_double_8_bytes << std::endl;
    std::cout << "long double: " << var_long_double_16_bytes << std::endl;

    std::cout << "float in bytes: " << sizeof(var_float_4_bytes) << std::endl;
    std::cout << "double in bytes: " << sizeof(var_double_8_bytes) << std::endl;
    std::cout << "long double in bytes: " << sizeof(var_long_double_16_bytes) << std::endl;

    /* NaN and +/- Infinity */

    float var_0 = 0;
    float var_x = -10;
    float var_nan = var_0 / var_0;
    float var_inf = var_x / var_0;

    std::cout << "var NaN: " << var_nan << std::endl;
    std::cout << "var NaN + 10: " << var_nan + 10 << std::endl;
    std::cout << "var NaN * 10: " << var_nan * 10 << std::endl;

    std::cout << "var Inf: " << var_inf << std::endl;
    std::cout << "var Inf - 10: " << var_inf - 10 << std::endl;
    
    /* Booleans */

    bool bool_false {false};    // {0} is valid too
    bool bool_true {true};      // {1} is valid too

    std::cout << std::boolalpha;  // Allow cout to print "true" and "false"
    std::cout << "bool false: " << bool_false << std::endl;
    std::cout << "bool true: " << bool_true << std::endl;

    /* Characters */

    char pepe {'a'};

    /* Auto datatype */

	auto var1 {12};
    auto var2 {13.0};
    auto var3 {14.0f};
    auto var4 {15.0l};
    auto var5 {'e'};
    
    /* int modifier suffixes */
    auto var6 {123u};          // unsigned
    auto var7 {123ul};         //unsigned long
    auto var8 {123ll};         // long long

    std::cout << "var1 occupies : " << sizeof(var1) << " bytes" << std::endl;
    std::cout << "var2 occupies : " << sizeof(var2) << " bytes" << std::endl;
    std::cout << "var3 occupies : " << sizeof(var3) << " bytes" << std::endl;
    std::cout << "var4 occupies : " << sizeof(var4) << " bytes" << std::endl;
    std::cout << "var5 occupies : " << sizeof(var5) << " bytes" << std::endl;
    std::cout << "var6 occupies : " << sizeof(var6) << " bytes" << std::endl;
    std::cout << "var7 occupies : " << sizeof(var7) << " bytes" << std::endl;
    std::cout << "var8 occupies : " << sizeof(var8) << " bytes" << std::endl;

    /* WARNING */
    auto var_auto {123u};  // initialized as an unsigned int value
    var_auto = -20;        // assigned with an signed int value

    std::cout << "var_auto: " << var_auto << std::endl; // ERROR

    return 0;
}