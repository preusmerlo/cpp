#include <iostream>

int main(int argc, char const *argv[])
{
    
    /* References always must be initialized */

    int var_int {15};
    int& r_var_int {var_int};
 
    std::cout << "var_int : " << var_int << std::endl;
    std::cout << "r_var_int : " << r_var_int << std::endl;

    std::cout << "&var_int : " << &var_int << std::endl;
    std::cout << "&r_var_int : " << &r_var_int << std::endl;
    
    std::cout << "sizeof var_int : " << sizeof(var_int) << std::endl;
    std::cout << "sizeof r_var_int : " << sizeof(r_var_int) << std::endl;

    std::cout << "=======================" << std::endl;

    /* When the user modifies the original variable, the reference copy the behavior */
    
    var_int = 22;

    std::cout << "var_int : " << var_int << std::endl;
    std::cout << "r_var_int : " << r_var_int << std::endl;

    std::cout << "&var_int : " << &var_int << std::endl;
    std::cout << "&r_var_int : " << &r_var_int << std::endl;
    
    std::cout << "sizeof var_int : " << sizeof(var_int) << std::endl;
    std::cout << "sizeof r_var_int : " << sizeof(r_var_int) << std::endl;

    std::cout << "=======================" << std::endl;

    /* When the user modifies the reference, the original variable copy the behavior */
    
    r_var_int = 100;

    std::cout << "var_int : " << var_int << std::endl;
    std::cout << "r_var_int : " << r_var_int << std::endl;

    std::cout << "&var_int : " << &var_int << std::endl;
    std::cout << "&r_var_int : " << &r_var_int << std::endl;
    
    std::cout << "sizeof var_int : " << sizeof(var_int) << std::endl;
    std::cout << "sizeof r_var_int : " << sizeof(r_var_int) << std::endl;

    std::cout << "=======================" << std::endl;

    /* References vs pointers
    
        Pointers:
        1) Use the desreference and reference operator to read and write data
        2) Can be changed to point to somewhere else
        3) Can be declared without initialization

        References:
        1) Dont use desreference operator to read and write
        2) Can not be changed to reference something else
        3) Can not be declared without initialization
    
    */

    /* Item 1 */

    double var_double {12.3};
    double& r_var_double {var_double};
    double* p_var_double {&var_double};

    std::cout << "var_double : " << var_double << std::endl;
    std::cout << "r_var_double : " << r_var_double << std::endl;
    std::cout << "p_var_double : " << p_var_double << std::endl;
    std::cout << "*p_var_double : " << *p_var_double << std::endl;

    std::cout << "=======================" << std::endl;

    /* Item 2 */

    double a {16.3};
    double b {20.0};

    double& r_a {a};
    double& r_b {b};

    r_b = r_a; /* Compila, pero modifica el valor de b(r_b) con el valor de a(r_a) */

    std::cout << "a   : " << a << std::endl;
    std::cout << "r_a : " << r_a << std::endl;
    std::cout << "b   : " << b << std::endl;
    std::cout << "r_b : " << r_b << std::endl;
    
    std::cout << "=======================" << std::endl;

    /* Item 3 */
    double var_double_x {100.3};
    double& r_var_double_x {var_double_x}; /* Unica forma valida */
    
    double* p_var_double_x {nullptr};
    p_var_double_x = &var_double_x;

    std::cout << "var_double : " << var_double_x << std::endl;
    std::cout << "r_var_double : " << r_var_double_x << std::endl;
    std::cout << "p_var_double : " << p_var_double_x << std::endl;
    std::cout << "*p_var_double : " << *p_var_double_x << std::endl;

    std::cout << "=======================" << std::endl;

    /* References are somewhat like const pointers
       (with the difference of the reference and dereference operators)
    */

    double var_double_y {120.120};
    double& r_var_double_y {var_double_y};
    double *const p_var_double_y {&var_double_y};

    std::cout << "var_double : " << var_double_y << std::endl;
    std::cout << "r_var_double : " << r_var_double_y << std::endl;
    std::cout << "p_var_double : " << p_var_double_y << std::endl;
    std::cout << "*p_var_double : " << *p_var_double_y << std::endl;

    std::cout << "=======================" << std::endl;

    /* Pointer to a reference */

    double var_double_w {500.2};                    // Variable
    double& r_var_double_w {var_double_w};          // Reference
    double *const p_var_double_w {&var_double_w};   // Pointer to variable
    double *const p_ref_double_w {&r_var_double_w}; // Pointer to reference

    std::cout << "var_double : " << var_double_w << std::endl;
    std::cout << "r_var_double : " << r_var_double_w << std::endl;
    std::cout << "p_var_double : " << p_var_double_w << std::endl;
    std::cout << "*p_var_double : " << *p_var_double_w << std::endl;

    *p_ref_double_w = 1000;

    std::cout << "var_double : " << var_double_w << std::endl;
    std::cout << "r_var_double : " << r_var_double_w << std::endl;
    std::cout << "p_var_double : " << p_var_double_w << std::endl;
    std::cout << "*p_var_double : " << *p_var_double_w << std::endl;

    /* Const reference */
    double var_double_t {102301.123};
    const double& r_var_double_t {var_double_t};

    // r_var_double_t = 100;    // ERROR
    // var_double_t = 100;      // VALID

    return 0;
}
