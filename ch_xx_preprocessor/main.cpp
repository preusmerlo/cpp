/*-----------------------------------------------------------------------
 * INCLUDE
 ----------------------------------------------------------------------*/

#include <iostream>
#include <string>
#include <map>

/*-----------------------------------------------------------------------
 * DEFINE
 ----------------------------------------------------------------------*/

#define CREATE(class, u_name) #class u_name(#u_name);
#define __DSPTx__(u_name) DSPTx u_name(#u_name);

#define ADD_TO_DICT(dict, var) __add_idem__(dict, #var, &var);
// #define ADD_TO_DICT(dict, var) dict[#var] = &var;
// #define ADD_TO_DICT(dict, var) dict.insert(std::pair<std::string, DSPTx*>(#var, &var));

/*-----------------------------------------------------------------------
 * CLASS: Module
 ----------------------------------------------------------------------*/

class SimBlock{
    private:
        std::map<std::string, void*> dict;

    public:
        SimBlock() = default;
        ~SimBlock() = default;

        template <typename T>
        void add_item_to_module_dict(std::string key, T* p_var);

};

template <typename T>
void SimBlock::add_item_to_module_dict(std::string key, T* p_var){
    this->dict[key] = static_cast<void*> p_var;
}

/*-----------------------------------------------------------------------
 * PROTOTIPES
 ----------------------------------------------------------------------*/

void __add_idem__(std::map<std::string, DSPTx*> &dict, std::string name, DSPTx* p_DSPTx);

/*-----------------------------------------------------------------------
 * MAIN
 ----------------------------------------------------------------------*/

int main(int argc, char const *argv[]){

    std::map<std::string, DSPTx*> dict;
    
    CREATE(DSPTx, pepe);

    __DSPTx__(p1)
    __DSPTx__(p2)
    __DSPTx__(p3)
    __DSPTx__(p4)

    ADD_TO_DICT(dict, p1);
    ADD_TO_DICT(dict, p2);
    ADD_TO_DICT(dict, p3);
    ADD_TO_DICT(dict, p4);

    std::cout << "Map size: " << dict.size() << std::endl;

    for(auto itr = dict.begin(); itr != dict.end(); ++itr){
        std::cout << itr->first << "(key): " << itr->second << " (value)" << std::endl;
    }

    return 0;
}

/*-----------------------------------------------------------------------
 * FUNCTIONS DEFINITION
 ----------------------------------------------------------------------*/

void __add_idem__(std::map<std::string, DSPTx*> &dict,
                  std::string name,
                  DSPTx* p_DSPTx){

    dict[name] = p_DSPTx;
}