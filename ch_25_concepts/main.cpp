#include <iostream>
#include <concepts>
#include <cmath>

/*  Integral types:
    1) int
    2) unsigned int
    3) short
    4) unsigned short
    5) long
    6) unsigned long
    7) long long
    8) unsigned long long
    9) char
    10) bool
*/

/* Syntax 1: good */
template <typename T>
requires std::is_integral_v<T> || std::is_floating_point_v<T>
T add(T a, T b){
    return a + b;
}

/* Syntax 2: best to only use one requirement */
template<std::integral T>
T pow(T a, T b){
    return std::pow(a, b);
}

/* Syntax 3: verbose but allow to combine concepts */
template<typename T>
concept IntegralOrFloatingPoint = std::is_integral_v<T> || std::is_floating_point_v<T>;

template <IntegralOrFloatingPoint T>
T power(T a, T b){
    return std::pow(a, b);
}

/* Syntax 4: without (explicit) template */
auto addX(IntegralOrFloatingPoint auto a, IntegralOrFloatingPoint auto b){
    return a + b;
}

/* Syntax 5: after function parametrs */
template <typename T>
T addY(T a, T b) requires std::integral<T> {
    return a + b;
}

/* Custom concepts */
template<typename T>
concept Integral = std::is_integral_v<T>;

template<typename T>
concept Multiplicable = requires(T a, T b){
    /* Just makes sure the syntax is valid */
    a * b;
};

template<typename T>
concept Incrementable = requires(T a){
    /* Just makes sure the syntax is valid */
    a+=1;
    ++a;
    a++;
};

/* Requires clause can take in four kinds or requirements:
    1) Simple requirements: only check syntax
    2) Nested requirements: checks if the expression is true
    3) Compound requirements: 
    4) Type requirements
 */

template <typename T>
concept TinyType = requires(T t){
    
    /* 1)  Simple requirements */
    sizeof(t) <= 4;

    /* 2) Nested requirements */
    requires sizeof(t) <= 4;

};

template <typename T>
concept Addable = requires (T a, T b){
    /* 3) Compound requirements */
    {a + b} noexcept -> std::convertible_to<int>;

    /* Checks if a + b is valid syntax, doesn't throw expetions (optional), and check
       if the result is convertible to int (optional)
    */
};


/* Main */
int main(int argc, char const *argv[])
{
 
    /* Integral types (char and int, among others) */

    char var_char_a {10};
    char var_char_b {20};

    auto var_char_c = add<char>(var_char_a, var_char_b);
    std::cout << "var_char_c: " << static_cast<int>(var_char_c) << std::endl;

    int var_int_a {10};
    int var_int_b {20};

    auto var_int_c = add<int>(var_int_a, var_int_b);
    std::cout << "var_char_c: " << var_int_c << std::endl;

    /* Not integral type (double, among others) */

    // double var_double_a {100.234};
    // double var_double_b {100.234};

    // auto var_double_c = add<double>(var_double_a, var_double_b); // ERROR: std::integral concep not satisfied.
    // std::cout << "var_char_c: " << var_double_c << std::endl;

    /* Concepts combination with template */
    double var_double_a {std::sqrt(2)};
    double var_double_b {2.0};

    auto var_double_c = power<double>(var_double_a, var_double_b);
    std::cout << "var_char_c: " << var_double_c << std::endl;

    /* Concepts combination without template */
    auto var_double_d = addX(var_double_a, var_double_b);
    std::cout << "var_char_d: " << var_double_d << std::endl;

    return 0;
}