#include <iostream>

int main(int argc, char const *argv[])
{
    
    /* Create Array */
    constexpr int SIZE {5};
    int scores [SIZE];

    /* Write Data */
    for(size_t i{}; i < SIZE; i++)
    {
        scores[i] = i;
    }

    /* Read Data */
    size_t i {0};
    for (auto score : scores)
    {
        std::cout << "score[" << i++ << "]: " << score  << std::endl;
    }

    std::cout << std::endl;

    for (size_t i {}; i < SIZE; i++)
    {
        std::cout << "score[" << i << "]: " << scores[i]  << std::endl;
    }
    
    std::cout << std::endl;

    /* Initialize an Array */
    
    float salaries [SIZE] {0.1f, 1.0f, 2.0f, 0.4f};

    for (size_t i {}; i < SIZE; i++)
    {
        std::cout << "salaries[" << i << "]: " << salaries[i]  << std::endl;
    }

    std::cout << std::endl;

    /* Initialize an Array with auto size */

    double auto_array [] {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
    unsigned int SIZE_AUTO {std::size(auto_array)};

    /*  for range without && makes a copy of each element of auto_array
        and doesnt allow the user to change the original value of the elements
        inside the array */

    for (auto value : auto_array) 
    {
        value *= 10;
    }
    
    for (size_t i {}; i < SIZE_AUTO; i++)
    {
        std::cout << "(original) auto_array[" << i << "]: " << auto_array[i]  << std::endl;
    }

    std::cout << std::endl;

    /*  for range WITH  && does not make a copy of each element of auto_array
        and allow the user to change the value of the elements inside the array */

    for (auto &&value : auto_array)
    {
        value *= 10;
    }

    for (size_t i {}; i < SIZE_AUTO; i++)
    {
        std::cout << "(updated) auto_array[" << i << "]: " << auto_array[i]  << std::endl;
    }

    /* Char an Array */

    char name_1 [] {'H', 'U', 'G', 'O', '\0'};              /* \0 puesto explicitamente */
    constexpr short int SIZE_CHAR_1 {std::size(name_1)};
    char name_2 [SIZE_CHAR_1] {'H', 'U', 'G', 'O'};           /* \0 puesto explicitamente */
    
    char name_3 [] {'H', 'U', 'G', 'O'};                    /* NO TIENE \0 al final */
    constexpr short int SIZE_CHAR_3 {std::size(name_3)};

    char name_4 [] {"HUGO"};                                /* SI TIENE \0 al final */
    constexpr short int SIZE_CHAR_4 {std::size(name_4)};

    /* Direct Print Out (ONLY VALID FOR CHAR ARRAYS) */

    std::cout << name_1 << std::endl;
    std::cout << name_2 << std::endl;
    std::cout << name_3 << std::endl;

    std::cout << name_1[SIZE_CHAR_1 - 1] << std::endl;    /* -> imprime \0 puesto explicitamente */
    std::cout << name_2[SIZE_CHAR_1 - 1] << std::endl;    /* -> imprime \0 puesto implicitamente */
    std::cout << name_3[SIZE_CHAR_3 - 1] << std::endl;    /* -> imprime 'O' en vez de \0 (porque no tiene) */
    std::cout << name_4[SIZE_CHAR_4 - 1] << std::endl;    /* -> imprime \0 puesto implicitamente con "text" */

    return 0;
}