#include <iostream>
#include "utils.h"
#include <string>

void increment(int*);
void increment(int&);
void max_int(int, int, int&);
std::string add(std::string, std::string);

int main(int argc, char const *argv[]){

    /* Function definition in other files */

    std::cout << "Suma (entera): " << sumar(10, 10) << std::endl;
    std::cout << "Suma (fraccional): " << sumar(10.2, 10.2) << std::endl;

    /* Pass data by value, pointer and reference */
    
    int value {10};
    int* p_value {&value};

    std::cout << "Variable (original) : " << value << std::endl;
    increment(p_value);
    std::cout << "Variable (incremented by pointer) : " << value << std::endl;
    increment(value);
    std::cout << "Variable (incremented by reference) : " << value << std::endl;

    /* Inputs and outputs (using input parameters) */
    int a {15};
    int b {12};
    int v_max {0};

    /* Return by reference */
    max_int(a, b, v_max);
    std::cout << "Max (from function) : " << v_max << std::endl;

    /* Return by pointer */
    std::string result_AA = add(std::string("Hola mundo"), std::string("Soy pepe"));
    std::cout << "Out: " << &result_AA << std::endl;

    /* Declare lambda function WITH NAME and THEN call it */
    auto func = [](){
        std::cout << "Hello World!" << std::endl;
    };
    func();

    /* Declare lambda function and call it FROM THE DECLARATION */
    [](){
        std::cout << "Hello World!" << std::endl;
    }();

    [](double a, double b){
        std::cout << "Result: " << a + b << std::endl;
    }(12.4, 13.2);

    /* Capture list of lambda function */

    double c {0};

    auto add_numbers = [&c](double a, double b){ // c (outside) could be afected by the lambda function
        c = a + b;
    };

    auto lambda_value_c = [c](){                // c (outside) can not be afected by the lambda function
        std::cout << "Value c (by value): " << c << std::endl;
    };

    lambda_value_c();
    std::cout << "Result: " << c << std::endl;
    add_numbers(2, 4);
    lambda_value_c();
    std::cout << "Result: " << c << std::endl;
    add_numbers(22, 45);
    lambda_value_c();
    std::cout << "Result: " << c << std::endl;

    /* Capture all by value (=) and by reference (&) */
    
    auto func_all_by_reference = [&](){
        /* all variables outside lambda are modificable */
    };

    auto func_all_by_value = [=](){
        /* all variables outside lambda are copied inside the lambda */
    };

    return 0;
}

void increment(int* a){
    (*a)++;
}

void increment(int& a){
    a++;
}

void max_int(int a, int b, int& v_max){
    v_max = b;
    if(a > b)
        v_max = a;
}

std::string add(std::string a, std::string b){
    std::string result = a + b;
    std::cout << "In: " << &result << std::endl;
    return result;
}