#include <iostream>
#include <string>
#include <map>

class Persona{
private:
    std::string nombre {"Nombre"};

public:
    Persona(std::string name);
    ~Persona() = default;

    std::string get_nombre() const;
    Persona* get_pointer();
};

Persona::Persona(std::string name){
    this->nombre = name;
}

std::string Persona::get_nombre() const {
    return this->nombre;
}

Persona* Persona::get_pointer(){
    return this;
}

void print_dict(auto dict);

int main(int argc, char const *argv[]){

    /* Creo algunos objetos */
    Persona persona_1("Juan");
    Persona persona_2("Carlos");
    Persona persona_3("Pedro");
    Persona persona_4("Lucas");
    
    /* Definicion de diccionario */
    std::map<std::string, Persona*> dict;

    /* Inserto objetos al diccionario:
        1) A traves de un std::pair
        2) A traves de un std::pair sin definirlo
        3) A traves del key
        4) Debe haber otra opcion que no se ...
    */

    /* 1) A traves de un std::pair */
    std::pair<std::string, Persona*> p;
    p.first = persona_1.get_nombre();
    p.second = persona_1.get_pointer();
    dict.insert(p);

    /* 2) A traves de un std::pair sin definirlo */
    dict.insert(std::pair<std::string, Persona*>(persona_2.get_nombre(), persona_2.get_pointer()));

    /* 3) A traves del key */
    dict[persona_3.get_nombre()] = persona_3.get_pointer();

    /* Metodo: size */
    std::cout << "Map size: " << dict.size() << std::endl;
    print_dict(dict);

    /* Metodo: erase */
    std::string key_to_remove {"Carlos"};
    dict.erase(key_to_remove);
    std::cout << std::endl;
    print_dict(dict);

    return 0;
}

void print_dict(auto dict){

    /* Metodos begin y end (ambos iterator) */
    for(auto itr = dict.begin(); itr != dict.end(); ++itr){
        std::cout << itr->first << "(key): " << itr->second << " (value)" << std::endl;
    }
}
